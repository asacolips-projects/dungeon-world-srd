module.exports = {
  // site config
  base: '/',
  dest: 'public',
  lang: 'en-US',
  title: 'Dungeon World SRD',
  description: 'DWSRD',

  // headers
  head: [
    ['meta', { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1' }],
  ],

  // theme and its config
  theme: '@vuepress/theme-default',
  themeConfig: {
    logo: '/images/dw-use-logo.jpg',
    darkMode: true,
    lastUpdated: true,
    docsDir: '/docs',
    navbar: [
      {
        text: 'Playing',
        children: [
          '/playing/',
          '/playing/playing-the-game',
          '/playing/example-of-play',
          '/playing/character-creation',
          '/playing/moves',
          '/playing/basic-moves',
          '/playing/special-moves',
        ]
      },
      {
        text: 'Classes',
        link: '/classes/',
        children: [
          '/classes/the-barbarian',
          '/classes/the-bard',
          '/classes/the-cleric',
          '/classes/the-cleric-spells',
          '/classes/the-druid',
          '/classes/the-fighter',
          '/classes/the-immolator',
          '/classes/the-paladin',
          '/classes/the-ranger',
          '/classes/the-thief',
          '/classes/the-wizard',
          '/classes/the-wizard-spells'
        ]
      },
      {
        text: 'Equipment',
        link: '/equipment/',
        children: [
          '/equipment/',
          '/equipment/tags',
          '/equipment/weapons',
          '/equipment/armor',
          '/equipment/dungeon-gear',
          '/equipment/poisons',
          '/equipment/expenses',
          '/equipment/magic-items'
        ]
      },
      {
        text: 'GMing',
        link: '/gm/',
        children: [
          '/gm/',
          '/gm/first-session',
          '/gm/fronts',
          '/gm/the-world',
          '/gm/advanced-delving'
        ]
      },
      {
        text: 'Monsters',
        link: '/monsters/',
        children: [
          '/monsters/',
          '/monsters/caverns',
          '/monsters/depths',
          '/monsters/experiments',
          '/monsters/folk',
          '/monsters/hordes',
          '/monsters/planes',
          '/monsters/swamp',
          '/monsters/undead',
          '/monsters/woods'
        ]
      },
      {
        text: 'Appendices',
        link: '/appendices/',
        children: [
          '/appendices/',
          '/appendices/basic-moves',
          '/appendices/special-moves',
          '/appendices/teaching',
          '/appendices/conversion',
          '/appendices/npcs'
        ]
      }
    ],
    sidebar: [
      {
        text: 'Playing',
        collapsible: true,
        children: [
          '/playing/',
          '/playing/playing-the-game',
          '/playing/example-of-play',
          '/playing/character-creation',
          '/playing/moves',
          '/playing/basic-moves',
          '/playing/special-moves',
        ]
      },
      {
        text: 'Classes',
        collapsible: true,
        children: [
          '/classes/the-barbarian',
          '/classes/the-bard',
          '/classes/the-cleric',
          '/classes/the-cleric-spells',
          '/classes/the-druid',
          '/classes/the-fighter',
          '/classes/the-immolator',
          '/classes/the-paladin',
          '/classes/the-ranger',
          '/classes/the-thief',
          '/classes/the-wizard',
          '/classes/the-wizard-spells'
        ]
      },
      {
        text: 'Equipment',
        collapsible: true,
        children: [
          '/equipment/',
          '/equipment/tags',
          '/equipment/weapons',
          '/equipment/armor',
          '/equipment/dungeon-gear',
          '/equipment/poisons',
          '/equipment/expenses',
          '/equipment/magic-items'
        ]
      },
      {
        text: 'GMing',
        collapsible: true,
        children: [
          '/gm/',
          '/gm/first-session',
          '/gm/fronts',
          '/gm/the-world',
          '/gm/advanced-delving'
        ]
      },
      {
        text: 'Monsters',
        collapsible: true,
        children: [
          '/monsters/',
          '/monsters/caverns',
          '/monsters/depths',
          '/monsters/experiments',
          '/monsters/folk',
          '/monsters/hordes',
          '/monsters/planes',
          '/monsters/swamp',
          '/monsters/undead',
          '/monsters/woods'
        ]
      },
      {
        text: 'Appendices',
        collapsible: true,
        children: [
          '/appendices/',
          '/appendices/basic-moves',
          '/appendices/special-moves',
          '/appendices/teaching',
          '/appendices/conversion',
          '/appendices/npcs'
        ]
      }
    ]
  },
  plugins: [
    [
      '@vuepress/plugin-search', {
        maxSuggestions: 15,
        getExtraFields: (page) => page.frontmatter.tags ?? [],
        locales: {
          '/': {
            placeholder: 'Search'
          }
        }
      }
    ],
    [
      '@vuepress/plugin-palette',
      {
        preset: 'scss',
        userPaletteFile: '.vuepress/styles/palette.scss',
        userStyleFile: '.vuepress/styles/index.scss',
        tempPaletteFile: 'styles/palette.scss',
        tempStyleFile: 'styles/index.scss',
        importCode: (filePath) => `@forward '${filePath}';\n`
      }
    ]
  ]
}