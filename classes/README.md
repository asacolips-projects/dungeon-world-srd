# Classes

- [The Barbarian](/classes/the-barbarian.md)
- [The Bard](/classes/the-bard.md)
- [The Cleric](/classes/the-cleric.md)
    - [Cleric Spells](/classes/the-cleric-spells.md)
- [The Druid](/classes/the-druid.md)
- [The Fighter](/classes/the-fighter.md)
- [The Immolator](/classes/the-immolator.md)
- [The Paladin](/classes/the-paladin.md)
- [The Ranger](/classes/the-ranger.md)
- [The Thief](/classes/the-thief.md)
- [The Wizard](/classes/the-wizard.md)
    - [Wizard Spells](/classes/the-wizard-spells.md)